https://github.com/rhildred/es6-twilio-chatbot

A chatbot written in es6 and vs6 for twilio and testing on the web. The important files are index.js and game.js.

A choose your own adventure for a haunted house presented as a Twilio chatbot. 

License
-----

MIT License: this code provides no warranty and no liability


How to run
-----

First, you have to execute the command on the terminal (only once):
    npm install

After that, to run type:
    npm start


|By: Renato Calado|
