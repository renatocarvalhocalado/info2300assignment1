const express = require('express');
const bodyParser = require("body-parser");
const Game = require("./Game");
const functions = require('firebase-functions');
const cors = require('cors')({origin: true});

// Create a new express application instance
const app = express();

app.use(cors);
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static("www"));

app.get("/users/:uname", (req, res) => {
    res.end("Hello " + req.params.uname);
});

let oGames = {};
app.get("/sms", (req, res) =>{
    let sFrom = req.body.From || req.body.from;
    if(!oGames.hasOwnProperty(sFrom)){
        oGames[sFrom] = new Game();
    }
    let sMessage = req.body.Body|| req.body.body;
    let aReply = oGames[sFrom].makeAMove(sMessage);
    res.setHeader('content-type', 'text/xml');
    let sResponse = "<Response>";
    for(let n = 0; n < aReply.length; n++){
        sResponse += "<Message>";
        sResponse += aReply[n];
        sResponse += "</Message>";
    }
    res.end(sResponse + "</Response>");
});


exports.app = functions.https.onRequest(app);